# Setup Script

This is a script that install some useful applications listed below. The objetive ofthis simple file is to make easier and faster the step of configurating a recently installed linux distro. At first, this is made for Debian 11 Bullseye.

Two steps are necessary before running it:

* Install git;
* Enter in `/etc/apt/sources.lst` and add:
  - "contrib non-free" at the end of all lines initiating with `deb`

The Following packages will be installed:

- wifi driver (firmware-iwlwifi)
- wget
- build-essential
- vim 
- codium
- telegram
- zoom
- jupyter notebook
- python libraries: numpy,scipy,matplotlib,sympy,pandas
- blender
