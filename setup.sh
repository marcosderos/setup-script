#!/bin/sh

su

apt update

#wifi
apt install firmware-iwlwifi 

#wget

apt-get install wget

apt-get install build-essential

#text editors
apt-get install vim
apt-get install codium

#----social-----

#telegram 
add-apt-repository ppa:atareao/telegram

apt-get install telegram-desktop

#zoom

wget https://zoom.us/client/latest/zoom_amd64.deb

apt install ./zoom_amd64.deb

rm zoom_amd64.deb

#python- basics for data analysis

apt-get install python-pip

pip install notebook
pip install numpy
pip install scipy
pip install matplotlib
pip install sympy
pip install pandas

#blender

apt-get install blender

#

